<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Andy Holmes | Web Developer & Designer')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->
    <script defer data-domain="andyholmes.me" src="https://plausible.io/js/plausible.outbound-links.js"></script>
</head>

<body class="">
    @include('sweetalert::alert')
    <header>
        <div class="container">
            <div class="row flex-column flex-column-reverse flex-md-row justify-content-between align-items-center">
                <div class="col-12 col-md-6 col-lg-5 col-xl-4 col-xxl-4">
                    <div class="nav-items d-flex justify-content-between align-items-center">
                        <a class="nav-items__item" href="{{ config('app.url') }}#intro">Intro</a>
                        <a class="nav-items__item" href="{{ config('app.url') }}#portfolio">Portfolio</a>
                        <a class="nav-items__item" href="{{ config('app.url') }}#skills">Skills</a>
                        <a class="nav-items__item" href="#contact">Contact</a>
                    </div>
                </div>
                <div class="mb-3 mb-md-0 col-6 col-md-4 col-lg-3 col-xl-2">
                    <div class="social-icons d-flex justify-content-between align-items-center">
                        <div class="social-icons__icon d-flex align-items-center justify-content-center">
                            <a rel="noopener" target="_blank" href="https://dribbble.com/andyjh07"><i class="fab fa-dribbble"></i></a>
                        </div>
                        <div class="social-icons__icon d-flex align-items-center justify-content-center">
                            <a rel="noopener" target="_blank" href="https://instagram.com/andyjh07"><i class="fab fa-instagram"></i></a>
                        </div>
                        <div class="social-icons__icon d-flex align-items-center justify-content-center">
                            <a rel="noopener" target="_blank" href="https://unsplash.com/@andyjh07"><i class="fab fa-unsplash"></i></a>
                        </div>
                        <div class="social-icons__icon d-flex align-items-center justify-content-center">
                            <a rel="noopener" target="_blank" href="https://linkedin.com/in/andyholmes7"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main role="main">
        <div id="app">
            @yield('content')
            <div id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-center text-lg-start">Contact Me.</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center text-lg-start">
                            <p class="contact__text">If you'd like to speak to me about a project you've got in mind or a job role
                                you think I'd be a good fit for, please feel free to email me at <a href="mailto:info@andyholmes.me?subject=Website%20Enquiry">info@andyholmes.me</a> and I'll get
                                back to you ASAP!</p>
                            <p class="contact__text">You may also be interested in viewing my CV, so please feel free to <a aria-label="Click to view Andy's CV PDF" target="_blank" href="{{ asset('files/Andy-Holmes-CV-2021.pdf')}}">view
                                    it</a> or <a download aria-label="Click to download Andy's CV PDF" href="{{ asset('files/Andy-Holmes-CV-2021.pdf')}}">download it (PDF File)</a>. I can provide
                                multiple references upon request.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="{{ asset('js/app.js') }}"></script>
    <footer>
        <div class="container">
            <div class="col-md-12 text-center">
                <p class="love d-flex align-items-center justify-content-center">Made with <i class="fas fa-heart fa-gradient"></i> in Chatham, Kent, UK.</p>
            </div>
        </div>
    </footer>
</body>

</html>