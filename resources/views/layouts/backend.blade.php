<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Andy Holmes | Web Developer & Designer')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body class="">
    @include('sweetalert::alert')
    <header>
        <div class="container">
            <div class="row flex-column flex-column-reverse flex-md-row justify-content-between align-items-center">
                <div class="col-10 col-md-6 col-lg-5 col-xl-4 col-xxl-2">
                    <div class="nav-items d-flex justify-content-between align-items-center">
                        <a class="nav-items__item" href="{{ route('dashboard') }}">Dashboard</a>
                        <a class="nav-items__item" href="{{ route('projects.index') }}">Projects</a>
                    </div>
                </div>
                <div class="mb-3 mb-md-0 col-4 col-lg-3 col-xl-3 text-end">
                    Welcome back, {{ auth()->user()->name }}!
                </div>
            </div>
        </div>
    </header>
    <main role="main">
        <div id="app">
            @yield('content')
        </div>
    </main>
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('footerScripts')
    <footer>
        <div class="container">
            <div class="col-md-12 text-center">
                <p class="love d-flex align-items-center justify-content-center">Made with <i class="fas fa-heart fa-gradient"></i> in Chatham, Kent, UK.</p>
            </div>
        </div>
    </footer>
</body>

</html>