<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Andy Holmes | Web Developer & Designer')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->
    <script defer data-domain="andyholmes.me" src="https://plausible.io/js/plausible.outbound-links.js"></script>
</head>

<body class="">
    <main role="main">
        {{ $slot }}
    </main>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>