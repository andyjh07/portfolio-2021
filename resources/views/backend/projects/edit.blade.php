@extends('layouts.backend')

@section('title', "Andy Holmes | Web Developer & Designer")

@section('content')
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="mt-8" action="{{ route('projects.update', $project->id) }}" method="post">
                @csrf
                @method('put')
                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-3">
                            <label for="title" class="form-label">Title</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ old('title') ?? $project->title }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mb-3">
                            <label for="subtitle" class="form-label">Subtitle</label>
                            <input type="text" class="form-control" id="subtitle" name="subtitle" value="{{ old('subtitle') ?? $project->subtitle }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mb-3">
                            <label for="studio" class="form-label">Studio</label>
                            <input type="text" class="form-control" id="studio" name="studio" value="{{ old('studio') ?? $project->studio }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mb-3">
                            <label for="skills-list" class="form-label">Skills (comma separated)</label>
                            <input type="text" class="form-control" id="skills-list" name="skills" value="{{ old('skills') ?? $project->clean_skills }}">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="website-url" class="form-label">URL</label>
                        <input type="text" class="form-control" id="website-url" name="url" value="{{ old('url') ?? $project->url }}">
                    </div>
                    <image-upload></image-upload>
                </div>
                <label for="quill_html" class="form-label">Description</label>
                <div id="quill_editor" style="height: 500px"></div>
                <input type="hidden" id="quill_html" value="" name="description" value="{{ old('description') ?? $project->description }}"></input>
                <input type="hidden" name="image" value="{{ old('image') ?? $project->image }}" id="imageFilename">
                <button type="submit" class="btn btn-info">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script>
    var quill = new Quill('#quill_editor', {
        theme: 'snow'
    });
    quill.on('text-change', function(delta, oldDelta, source) {
        document.getElementById("quill_html").value = quill.root.innerHTML;
    });
    // quill.setContents("<p>123</p>");
    quill.root.innerHTML = "{!! old('description') ?? $project->description !!}";
</script>
@endsection