@extends('layouts.app')

@section('title', "Andy Holmes | Web Developer & Designer")

@section('content')
    <div id="intro" class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-xl-12">
                <div class="intro-heading text-center">
                    <div class="intro-heading__title">
                        <span class="block"></span>
                        <h1>{{ $project->title }}</h1>
                    </div>
                    <div class="intro-heading__role">
                        <span class="block"></span>
                        <p>{{ $project->subtitle }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="profile">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="profile-inner">
                        <img class="w-100" src="{{ asset("images/{$project->image}") }}" alt="Preview image of the {{ $project->name }} project">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="url">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    Visit Website: <a target="_blank" href="{{ $project->url }}">{{ $project->url }}</a>
                </div>
            </div>
        </div>
    </div>
    <div id="project">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center text-lg-start">The Project.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center text-lg-start">
                    {!! $project->description !!}
                </div>
            </div>
        </div>
    </div>
    <div id="skills">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center text-lg-start">Skills Used.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="skills-list d-flex flex-wrap justify-content-center justify-content-lg-start align-items-center flex-column flex-lg-row">
                        @foreach($project->skills as $skill)
                        <li>{{ $skill }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection