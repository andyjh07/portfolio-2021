@extends('layouts.app')

@section('title', "Andy Holmes | Web Developer & Designer")

@section('content')
<div id="intro" class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-xl-8">
            <div class="intro-heading text-center">
                <div class="intro-heading__title">
                    <span class="block"></span>
                    <h1>Hey, I'm Andy.</h1>
                </div>
                <div class="intro-heading__role">
                    <span class="block"></span>
                    <p>A full stack developer (and occasional designer) from Kent.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="profile">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="profile-inner d-flex flex-column flex-column-reverse flex-lg-row align-items-center align-items-lg-start align-items-xl-end align-items-xxl-center justify-content-center">
                    <img class="profile-inner__image" src="{{ asset('images/Andy-Holmes-Headshot.jpg') }}" alt="Headshot image of Andy">
                    <div class="profile-inner__text">
                        <p>
                            I built my first website at the age of 11 using a website builder called Homestead. I quickly progressed into learning HTML & CSS when I was around 13 and then started learning PHP & MySQL at 16 years old.
                        </p>
                        <p>
                            I have a passion for learning and have taught myself everything I know. On top of web development & design, I’m also a <a rel="noopener" target="_blank" href="https://www.instagram.com/andyjh07/">Photographer</a> and a bit of a <span class="informative">Linguaphile<span class="tooltiptext">A <em>linguaphile</em> is a person who loves language and words.</span></span>; learning a variety of different languages on <a rel="noopener" target="_blank" href="https://www.duolingo.com/profile/andyjh07">Duolingo</a>. I’m always looking to better myself in every aspect and have no problem throwing myself in at the deep end if required.
                        </p>
                        <p>
                            My previous colleagues have described me as a real hybrid or “Unicorn developer” due to my interest & experience in design and in various parts of a business’ lifecycle/processes. Code will always be what makes me tick though, I absolutely love building things and coming up with clever solutions to problems.
                        </p>
                        <p>
                            I pride myself on being a super friendly and approachable person, a real team-player who has no problem getting involved in discussions or meetings. I have over <strong>8 years professional experience</strong> in a variety of project types and can slot into most roles quickly.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center text-lg-start">Portfolio.</h2>
            </div>
        </div>
        <div class="row">
            @forelse($projects as $project)
            <div class="col-md-6 col-lg-4">
                <div class="portfolio-item">
                    <a href="{{ route('projects.show', $project->slug) }}"><img src="{{ asset("images/{$project->image}") }}" alt="Description of image" class="portfolio-item__image"></a>
                    <div class="portfolio-item__content">
                        <h3><a href="{{ route('projects.show', $project->slug) }}">{{ $project->title }}</a></h3>
                        <p>Studio: {{ $project->studio }}</p>
                        <ul class="tags d-flex align-items-center justify-content-start flex-wrap">
                            @foreach($project->skills->take(4) as $skill)
                            <li>{{ $skill }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @empty
            <div class="col-12 text-center">
                <p>I'm working on adding some projects into my portfolio, please bear with me!</p>
            </div>
            @endforelse
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-center">As this is a new portfolio, I've added {{ $projects->count() }} out of 15 projects so far. Please bear with me whilst I finish populating my site.</p>
            </div>
        </div>
    </div>
</div>
<div id="skills">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center text-lg-start">Skills.</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="skills-list d-flex flex-wrap justify-content-center justify-content-lg-start align-items-center flex-column flex-md-row">
                    <li>Photoshop</li>
                    <li>Sketch</li>
                    <li>HTML</li>
                    <li>CSS/SCSS</li>
                    <li>JavaScript</li>
                    <li>jQuery</li>
                    <li>Vue.js 2+</li>
                    <li>AJAX</li>
                    <li>PHP</li>
                    <li>Laravel v5.1+</li>
                    <li>Accessibility</li>
                    <li>Bootstrap v3+</li>
                    <li>Tailwind CSS</li>
                    <li>MySQL</li>
                    <li>API Experience</li>
                    <li>Version Control</li>
                    <li>cPanel Management</li>
                    <li>WordPress ACF</li>
                    <li>WordPress Custom Themes</li>
                    <li>ExpressionEngine v2+</li>
                    <li>Project Management</li>
                    <li>Deploy HQ</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection