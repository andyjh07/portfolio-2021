require('./bootstrap');
import 'bootstrap';
import ImageUpload from "./components/ImageUpload";

import { createApp } from 'vue';

createApp({
    components: {
        ImageUpload
    }
}).mount('#app');