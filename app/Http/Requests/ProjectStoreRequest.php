<?php

namespace App\Http\Requests;

use Illuminate\Support\Str;
use Illuminate\Foundation\Http\FormRequest;

class ProjectStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->has('title'))
            $this->merge(['slug' => Str::slug($this->title)]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|unique:projects',
            'slug' => 'required_with:name|unique:projects',
            'subtitle' => 'required|string',
            'description' => 'required|string',
            'studio' => 'required|string',
            'skills' => 'required|string',
            'url' => 'required|string',
            'image' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'slug.unique' => 'An amenity already exists with this identifier. Please try using a different name.',
        ];
    }
}
