<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'subtitle',
        'slug',
        'studio',
        'description',
        'skills',
        'image',
        'url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['clean_skills'];

    public function getSkillsAttribute($value)
    {
        return collect(explode(', ', $value));
    }

    public function getCleanSkillsAttribute()
    {
        return $this->getRawOriginal('skills');
    }
}
