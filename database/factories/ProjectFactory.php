<?php

namespace Database\Factories;

use App\Models\Project;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->words(3, true);

        return [
            'title' => "Reptimate - Genetics Calculator",
            'subtitle' => "A PHP based genetics calculator for Leopard Geckos & Fat Tailed Geckos.",
            'slug' => Str::slug("Reptimate - Genetics Calculator"),
            'studio' => 'Freelance',
            'description' => 'This is a description',
            'skills' => "HTML, SCSS, Laravel 5.1, Bootstrap 4",
            'image' => "reptimate.jpg",
            'url' => "https://www.reptimatecalculator.com",
        ];
    }
}
