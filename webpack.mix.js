const mix = require('laravel-mix');
require('laravel-mix-purgecss');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/@fortawesome/fontawesome-pro/webfonts', 'public/webfonts')
    .js("resources/js/app.js", "public/js").vue()
    .sass("resources/scss/app.scss", "public/css")
    .options({
        postCss: [
            require('postcss-nested'), // or require('postcss-nesting')
            require('autoprefixer'),
        ]
    }).purgeCss({
       enabled: true,
   });
    