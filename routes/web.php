<?php

use App\Models\Project;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index', [
        'projects' => Project::orderByDesc('created_at')->get()
    ]);
});


Route::get('project/{project:slug}', [ProjectController::class, 'show'])->name('projects.show');

Route::prefix('admin')->middleware('auth', 'verified')->group(
    function () {
        Route::get('dashboard', function () {
            return view('dashboard');
        })->name('dashboard');

        Route::post('projects/image/upload', [ProjectController::class, 'upload'])->name('projects.image.upload');

        Route::resource('projects', ProjectController::class)->except([
            'show'
        ]);
    }
);

require __DIR__ . '/auth.php';
